const { Client } = require('@googlemaps/google-maps-services-js');

const client = new Client({});

client
	.reverseGeocode({
		params: {
			latlng: [44.475712299152875, 7.363641412150628],
			key: 'AIzaSyAbHSxXy67QyEKEdJLXpWDn85yHSNsMuII',
		},
		timeout: 1000, // milliseconds
	})
	.then((r) => {
		let country_code = '';
		let party_id = '';
		let location_id = '';

		r.data.results[0].address_components.forEach((element) => {
			if (element.types.includes('country')) {
				country_code = element.short_name;
				console.log('-- ', element.short_name);
			}
			if (element.types.includes('administrative_area_level_1')) {
				party_id = element.short_name;
				console.log('-- ', element.short_name);
			}
			if (element.types.includes('administrative_area_level_3')) {
				location_id = element.short_name;
				console.log('-- ', element.short_name);
			}
		});
		console.log('Path ', country_code + '/' + party_id + '/' + location_id);
	})
	.catch((e) => {
		console.log(e);
	});
