const http = require('https');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	//console.log(`*** ${code} - ${bodyStr} ***`);
	let header = {
		'X-Total-Count': 50,
		'X-Limit': 10,
		'Content-Type': 'application/json',
		'Content-Length': Buffer.byteLength(bodyStr),
	};
	return {
		statusCode: code,
		headers: header,
		body: bodyStr,
	};
};

const request = (payload, body) =>
	new Promise((resolve, reject) => {
		let postData = JSON.stringify(body);
		let req = http.request(payload, (res) => {
			let output = '';
			res.on('data', (chunk) => (output += chunk));
			res.on('end', () => {
				console.log(`request sent to client`);
				console.log(`${res.statusCode} - ${res.statusMessage} - ${output}`);
				resolve(output);
			});
		});
		req.on('error', (e) => {
			console.log(`Error while sending an request to the client API endpoint`);
			console.log(e);
			reject(e);
		});
		req.write(postData);
		req.end();
	})
		.then(function (data) {
			return data;
		})
		.catch(function (err) {
			console.log('Err.', err);
		});

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	//let filter = JSON.parse(event);
	//console.log(filter);
	let command = '';
	let body = { result: 'ACCEPTED' };
	let payload = {
		method: 'POST',
		hostname: event.response_url,
	};
	let result = await request(payload, body);

	return httpResult(200, result);
};
