const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.getOccupiedConnectors = async (chargebox) => {
	let params = {
		TableName: process.env.DYNAMODB_TRANSACTIONS_RUNNING,
		KeyConditionExpression: '#chargebox = :chargebox',
		FilterExpression: '#started = :true',
		ProjectionExpression: '#connector',
		ExpressionAttributeNames: {
			'#chargebox': 'chargebox',
			'#started': 'started',
			'#connector': 'connector',
		},
		ExpressionAttributeValues: {
			':chargebox': chargebox,
			':true': true,
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.query(params).promise();
	return (data.Items || []).map((i) => i.connector);
};
