const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.store = async (data) => {
	return new Promise((resolve, reject) => {
		const params = {
			TableName: process.env.DYNAMODB_OCPI_CHARGERS,
			Item: data,
			ReturnConsumedCapacity: 'NONE',
			ReturnItemCollectionMetrics: 'NONE',
			ReturnValues: 'NONE',
		};

		DynamoDB.put(params)
			.promise()
			.then(resolve)
			.catch((err) => {
				if (err.code === 'ConditionalCheckFailedException') {
					reject('uuid_already_exists');
				} else {
					reject(err);
				}
			});
	});
};

exports.getUUID = async (id) => {
	let params = {
		TableName: process.env.DYNAMODB_OCPI_CHARGERS,
		Key: { id },
		ProjectionExpression: '#uuid',
		ExpressionAttributeNames: {
			'#uuid': 'uuid',
		},
	};
	let data = await DynamoDB.get(params).promise();
	let result = data.Item && data.Item.uuid ? data.Item.uuid : null;
	return result;
};
