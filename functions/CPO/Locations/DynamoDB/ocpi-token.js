const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.add = async (data) => {
	return new Promise((resolve, reject) => {
		const params = {
			TableName: process.env.DYNAMODB_OCPI_TOKEN,
			Item: data,
			ReturnConsumedCapacity: 'NONE',
			ReturnItemCollectionMetrics: 'NONE',
			ReturnValues: 'NONE',
		};

		DynamoDB.put(params)
			.promise()
			.then(resolve)
			.catch((err) => {
				if (err.code === 'ConditionalCheckFailedException') {
					reject('token_already_exists');
				} else {
					reject(err);
				}
			});
	});
};

module.exports.scanType = async (type) => {
	let params = {
		TableName: process.env.DYNAMODB_OCPI_TOKEN,
		FilterExpression: '#type = :type',
		ProjectionExpression: '#id, #type',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#type': 'type',
		},
		ExpressionAttributeValues: {
			':type': type,
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.scan(params).promise();
	console.log('Data', data.Items);
	return data.Items || [];
};
exports.get = async (id) => {
	let params = {
		TableName: process.env.DYNAMODB_OCPI_TOKEN,
		Key: { id },
		ProjectionExpression: '#id,#type',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#type': 'type',
		},
	};
	let data = await DynamoDB.get(params).promise();
	return data.Item || null;
};
module.exports.delete = async function (token) {
	let params = {
		TableName: process.env.DYNAMODB_OCPI_TOKEN,
		Key: {
			id: token,
		},
		ReturnConsumedCapacity: 'NONE',
		ReturnItemCollectionMetrics: 'NONE',
		ReturnValues: 'NONE',
	};
	return DynamoDB.delete(params).promise();
};
