const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBOcpiToken = require('./DynamoDB/ocpi-token');
const DynamoDBOcpiChargers = require('./DynamoDB/ocpi-chargers');
const http = require('http');
const ShortUniqueId = require('short-unique-id');

const DynamoDBClients = require('./DynamoDB/clients');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	//console.log(`*** ${code} - ${bodyStr} ***`);
	let header = {
		'X-Total-Count': 50,
		'X-Limit': 10,
		'Content-Type': 'application/json',
		'Content-Length': Buffer.byteLength(bodyStr),
	};
	return {
		statusCode: code,
		headers: header,
		body: bodyStr,
	};
};

const request = (payload, body) =>
	new Promise((resolve, reject) => {
		let postData = JSON.stringify(body);
		let req = http.request(payload, (res) => {
			let output = '';
			res.on('data', (chunk) => (output += chunk));
			res.on('end', () => {
				console.log(`request sent to client`);
				console.log(`${res.statusCode} - ${res.statusMessage} - ${output}`);
				resolve(output);
			});
		});
		req.on('error', (e) => {
			console.log(`Error while sending an request to the client API endpoint`);
			console.log(e);
			reject(e);
		});
		req.write(postData);
		req.end();
	})
		.then(function (data) {
			return JSON.parse(data);
		})
		.catch(function (err) {
			console.log('Err.', err);
		});

const getEvse = async (cb, evse) => {
	let chargebox = await DynamoDBChargeboxes.get(cb);
	if (chargebox === null) {
		return httpResult(404, { message: 'ChargeboxNotFound' });
	}
	let connector = chargebox.connectors[evse - 1];
	let result = {
		uid: evse,
		status: 'AVAILABLE',
		connectors: await getConnector(connector, evse - 1, new Date(chargebox.last_heartbeat).toISOString()),
		last_updated: new Date(chargebox.last_heartbeat).toISOString(),
	};
	return result;
};

const getEvses = async (cb) => {
	let chargebox = await DynamoDBChargeboxes.get(cb);
	if (chargebox === null) {
		return httpResult(404, { message: 'ChargeboxNotFound' });
	}
	let uuid = await DynamoDBOcpiChargers.getUUID(cb);
	if (!uuid || uuid.length < 1 || uuid === null) {
		let uid_tmp = new ShortUniqueId({ length: 6 });
		uid_tmp.setDictionary('alphanum_upper');
		let uid = uid_tmp();
		let data = {
			id: cb,
			uuid: uid,
		};
		await DynamoDBOcpiChargers.store(data);
		uuid = uid;
	}

	let connectors = chargebox.connectors;
	let evses = await Promise.all(
		connectors.map(async (connector, key) => {
			let conn = key + 1;
			let result = {
				uid: key + 1,
				evse_id: 'IT*THO*E' + uuid + '*' + conn,
				status: 'AVAILABLE',
				capabilities: ['REMOTE_START_STOP_CAPABLE', 'RESERVABLE', 'RFID_READER', 'UNLOCK_CAPABLE'],
				connectors: await getConnector(connector, key, new Date(chargebox.last_heartbeat).toISOString()),
				last_updated: new Date(chargebox.last_heartbeat).toISOString(),
			};
			return result;
		})
	);
	return evses;
};
const getConnector = async (connector, key, last_updated) => {
	let connectorType;
	let phase = 'tri';
	switch (connector.type) {
		case 'chademo':
			connectorType = 'CHADEMO';
			phase = 'mono';
			break;
		case 'shuko':
			connectorType = 'DOMESTIC_F';
			phase = 'mono';
			break;
		case 'type_1':
			connectorType = 'IEC_62196_T1';
			break;
		case 'commando':
			connectorType = 'IEC_60309_2_single_16';
			break;
		case 'type_2':
			connectorType = 'IEC_62196_T2';
			break;
		default:
			break;
	}
	let connectorResult = {
		id: key + 1,
		standard: connectorType,
		format: connector.format ? connector.format : 'SOCKET',
		power_type: connector.power_type ? connector.power_type : 'AC_3_PHASE',
		max_voltage: phase === 'mono' ? '230' : '400',
		max_electric_power: connector.power ? connector.power : '10000',
		max_amperage: connector.max_amperage ? connector.max_amperage : '32',
		last_updated: last_updated,
	};
	return connectorResult;
};

const filterAddress = async (array, filter) => {
	let addressesFiltered = array.filter((r) => r.types.includes(filter));
	return addressesFiltered;
};

const getChargebox = async (cb) => {
	try {
		let chargebox = await DynamoDBChargeboxes.get(cb);
		console.log('Chargebox', chargebox);
		let item = {
			country_code: 'IT',
			party_id: 'THO',
			id: chargebox.id,
			publish: chargebox.public,
			name: chargebox.alias,
			address: chargebox.address ? chargebox.address : 'undefined',
			city: chargebox.city ? chargebox.city : 'undefined',
			country: 'ITA',
			coordinates: chargebox.coordinates,
			time_zone: chargebox.timezone,
			last_updated: chargebox.last_heartbeat ? new Date(chargebox.last_heartbeat).toISOString() : new Date().toISOString(),
			evse: await getEvses(chargebox.id),
		};
		return item;
	} catch (e) {
		console.log('Error', e);
		httpResult(500, { message: 'InternalServerError getChargebox' });
	}
};

const getChargeboxes = async (onlyActive) => {
	let chargeboxes = [];
	chargeboxes = await DynamoDBChargeboxes.getByClient(null, onlyActive);

	let result = await Promise.all(
		chargeboxes.map(async (chargebox) => {
			try {
				let item = [];
				if (chargebox.last_heartbeat) {
					item = await getChargebox(chargebox.id);
				}
				return item;
			} catch (e) {
				httpResult(500, { message: 'InternalServerError getChargeboxes' });
			}
		})
	);
	//console.log('Resultato ::: ', result);
	return result;
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	if (event.httpMethod == 'GET') {
		let token_in = event.headers.Authorization;
		try {
			console.log('IN', token_in);
			let tk_in = token_in.split(' ');
			let token = await DynamoDBOcpiToken.get(tk_in[1]);
			//console.log('TOEK', token);
			if (!token || token.type !== 'IN') {
				return httpResult(3001, { message: 'Token Error' });
			}
			let onlyActive = true;
			let result = null;
			if (event.pathParameters && event.pathParameters.location_id && event.pathParameters.evse_id && event.pathParameters.connector_id && !result) {
				let chargebox = await DynamoDBChargeboxes.get(event.pathParameters.location_id);
				result = await getConnector(
					event.pathParameters.location_id,
					event.pathParameters.connector_id - 1,
					new Date(chargebox.last_heartbeat).toISOString()
				);
			}
			if (event.pathParameters && event.pathParameters.location_id && event.pathParameters.evse_id && !result) {
				result = await getEvse(event.pathParameters.location_id, event.pathParameters.evse_id);
			}
			if (event.pathParameters && event.pathParameters.location_id && !result) {
				result = await getChargebox(event.pathParameters.location_id);
			}
			if (result) {
				return httpResult(200, result);
			}
			return httpResult(200, await getChargeboxes(onlyActive));
		} catch (err) {
			console.log(err);
			return httpResult(500, { message: 'InternalServerError' });
		}
	}
	if (event.httpMethod == 'PATCH') {
		let token_out = await DynamoDBOcpiToken.scanType('OUT');
		if (!token_out[0].id || token_out[0].type !== 'OUT') {
			return httpResult(3001, { message: 'Token Error' });
		}
		let versions_payload = {
			method: event.httpMethod,
			port: 8080,
			hostname: 'cpo-testing.evway.net',
			path: '/ocpi/emsp/2.1.1/locations/IT/THO/' + event.chargebox + '/' + event.body.connectorId + '/' + event.body.connectorId,
			headers: {
				Authorization: 'Token ' + token_out[0].id,
				'Content-Type': 'application/json',
			},
		};
		let body = {
			status: event.body.status,
			last_updated: new Date(event.body.timestamp).toISOString(),
		};
		let versions = await request(versions_payload, body);
	}
};
