const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.getAll = async () => {
	let params = {
		TableName: process.env.DYNAMODB_CLIENTS,
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.scan(params).promise();
	console.log('Data', data);
	return data.Items || [];
};

exports.getByAPIKey = async (apikey) => {
	let params = {
		TableName: process.env.DYNAMODB_CLIENTS,
		IndexName: 'apikey-index',
		KeyConditionExpression: '#api_key = :api_key',
		ProjectionExpression: '#id, #rest_api',
		ExpressionAttributeNames: {
			'#api_key': 'api_key',
			'#id': 'id',
			'#rest_api': 'rest_api',
		},
		ExpressionAttributeValues: {
			':api_key': apikey,
		},
	};

	let data = await DynamoDB.query(params).promise();
	if (data && Array.isArray(data.Items) && data.Items.length > 0) {
		return data.Items[0] || null;
	}
	return null;
};
