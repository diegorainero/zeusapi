const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBTransactionsFinished = require('./DynamoDB/transactions-finished');
const DynamoDBClients = require('./DynamoDB/clients');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	console.log(`*** ${code} - ${bodyStr} ***`);
	let header = {
		'X-Total-Count': 50,
		'X-Limit': 10,
		'Content-Type': 'application/json',
		'Content-Length': Buffer.byteLength(bodyStr),
	};
	return {
		statusCode: code,
		headers: header,
		body: bodyStr,
	};
};

const getPaymentInfos = async (filter) => {
	let clients = await DynamoDBClients.getAll();
	let items = [];
	if (clients.length > 0) {
		clients.forEach((client) => {
			items.push({
				country_code: 'IT',
				party_id: 'THO',
				id: client.id,
				currency: client.payment_limit.currency,
				elements: [
					{
						type:
							client.payment_type.type === 'free' || client.payment_type.type === 'flat'
								? 'FLAT'
								: client.payment_type.type === 'time'
								? 'TIME'
								: 'ENERGY',
						price: client.payment_type.price ? client.payment_type.type : 0,
						step_size: 1,
					},
				],
				last_updated: new Date().toISOString(),
			});
		});
	}
	return items;
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	let filter = event.queryStringParameters;
	try {
		return httpResult(200, await getPaymentInfos(filter));
	} catch (err) {
		console.log(err);
		return httpResult(500, { message: 'InternalServerError' });
	}
};
