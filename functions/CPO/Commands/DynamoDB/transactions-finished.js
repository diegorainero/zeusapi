const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.getFinishedTransactions = async (filter) => {
	console.log('Filter', filter);
	let params = {
		TableName: process.env.DYNAMODB_TRANSACTIONS_FINISHED,
		ExpressionAttributeNames: {},
		ExpressionAttributeValues: {},
		ReturnConsumedCapacity: 'NONE',
	};
	if (filter) {
		if (filter.date_from && filter.date_to) {
			params.FilterExpression = '#date_finished > :from and #date_finished <= :to';
			params.ExpressionAttributeNames['#date_finished'] = 'date_finished';
			params.ExpressionAttributeValues[':from'] = new Date(filter.date_from).getTime();
			params.ExpressionAttributeValues[':to'] = new Date(filter.date_to).getTime();
		}
		if (filter.date_from && !filter.date_to) {
			params.FilterExpression = '#date_finished > :from';
			params.ExpressionAttributeNames['#date_finished'] = 'date_finished';
			params.ExpressionAttributeValues[':from'] = new Date(filter.date_from).getTime();
		}
		if (filter.offset !== null) {
			params.ExclusiveStartKey = filter.offset;
		}
		if (filter.limit !== null) {
			params.Limit = filter.limit;
		}
	}
	console.log('Params', params);
	let data = await DynamoDB.scan(params).promise();
	return data.Items || [];
};

exports.getTransactionCdrs = async (id) => {
	let params = {
		TableName: process.env.DYNAMODB_TRANSACTIONS_FINISHED,
		Key: { id },
		ProjectionExpression:
			'#id, #date_started, #status, #user , #transaction_id, #chargebox, #reason, #client, #date_finished, #has_invoice, #id_tag, #payment, #meter_stop, #meter_start, #type',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#date_started': 'date_started',
			'#status': 'status',
			'#user': 'user',
			'#transaction_id': 'transaction_id',
			'#chargebox': 'chargebox',
			'#reason': 'reason',
			'#client': 'client',
			'#date_finished': 'date_finished',
			'#has_invoice': 'has_invoice',
			'#id_tag': 'id_tag',
			'#payment': 'payment',
			'#meter_stop': 'meter_stop',
			'#meter_start': 'meter_start',
			'#type': 'type',
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.get(params).promise();
	return data.Item || [];
};
