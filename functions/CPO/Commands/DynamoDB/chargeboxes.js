const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.get = async (id) => {
	let params = {
		TableName: process.env.DYNAMODB_CHARGEBOXES,
		Key: { id },
		ProjectionExpression: '#id, #active, #alias, #coordinates, #type, #connectors, #connected, #last_heartbeat, #timezone, #public',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#active': 'active',
			'#alias': 'alias',
			'#coordinates': 'coordinates',
			'#type': 'type',
			'#connectors': 'connectors',
			'#connected': 'connected',
			'#last_heartbeat': 'last_heartbeat',
			'#timezone': 'timezone',
			'#public': 'public',
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.get(params).promise();
	return data.Item || null;
};
