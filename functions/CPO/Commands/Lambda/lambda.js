const AWS = require('aws-sdk');
const Lambda = new AWS.Lambda({
	maxRetries: parseInt(process.env.LAMBDA_RETRIES),
	retryDelayOptions: parseInt(process.env.LAMBDA_RETRY_DELAY_MILLISECONDS),
});

module.exports.invoke = (name, data) => {
	let functionName = `${process.env.LAMBDA_FUNCTION_PREFIX}${name}`;
	const params = {
		FunctionName: functionName,
		Payload: JSON.stringify(data),
	};

	return Lambda.invoke(params).promise();
};
