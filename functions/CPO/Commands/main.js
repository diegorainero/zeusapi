const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBTransactionsFinished = require('./DynamoDB/transactions-finished');
//const DynamoDBClients = require('./DynamoDB/clients');
const Lambda = require('./Lambda/lambda');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	console.log(`*** ${code} - ${bodyStr} ***`);
	let header = {
		'X-Total-Count': 50,
		'X-Limit': 10,
		'Content-Type': 'application/json',
		'Content-Length': Buffer.byteLength(bodyStr),
	};
	return {
		statusCode: code,
		headers: header,
		body: bodyStr,
	};
};

const getFinishedTransactions = async (filter) => {
	let transactions = await DynamoDBTransactionsFinished.getFinishedTransactions(filter);
	let chargeboxes = await DynamoDBChargeboxes.getAll();
	let charges = [];
	chargeboxes.forEach((charge) => {
		charges[charge.id] = charge;
	});
	let chargeSelected = {};
	let items = [];
	if (transactions.length > 0) {
		transactions.forEach((transaction) => {
			let datasCharge = transaction.id.split('__');
			let connector = datasCharge[1];
			chargeSelected = charges[transaction.chargebox];
			if (!chargeSelected) {
				return;
			}
			items.push({
				country_code: 'IT',
				party_id: 'THO',
				id: transaction.id,
				start_date_time: new Date(transaction.date_started),
				end_date_time: new Date(transaction.date_finished),
				cdr_token: transaction.id_tag,
				auth_method: 'WHITELIST',
				cdr_location: {
					id: chargeSelected.id,
					name: chargeSelected.alias,
					address: chargeSelected.address,
					city: chargeSelected.city,
					postal_code: chargeSelected.postal_code ? chargeSelected.postal_code : '000',
					country: chargeSelected.country,
					coordinates: chargeSelected.coordinates,
					evse_uid: connector,
					evse_id: connector,
					connector_id: connector,
					connnector_standard:
						chargeSelected.connectors[connector] && chargeSelected.connectors[connector].type ? chargeSelected.connectors[connector].type : '',
					connector_format:
						chargeSelected.connectors[connector] && chargeSelected.connectors[connector].format ? chargeSelected.connectors[connector].format : '',
					connector_power_type:
						chargeSelected.connectors[connector] && chargeSelected.connectors[connector].power_type
							? chargeSelected.connectors[connector].power_type
							: '',
					uid: '',
					type: '',
					contract_id: 'ITTHO',
				},
				currency: transaction.payment && transaction.payment.currency ? transaction.payment.currency : '',
				total_cost: transaction.payment && transaction.payment.price ? transaction.payment.price : '',
				total_energy: transaction.meter_stop - transaction.meter_start,
				total_time: (transaction.date_finished - transaction.date_started) / (1000 * 60 * 60),
				last_updated: new Date(transaction.date_finished),
			});
		});
	}
	return items;
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	let filter = JSON.parse(event.body);
	console.log(filter);
	let command = '';
	let payload = {};
	if (event.path.includes('START_SESSION')) {
		let chargebox = await DynamoDBChargeboxes.get(filter.location_id);
		console.log('Chargebox', chargebox);

		if (chargebox.type.msg === 'soap' && chargebox.type.version === '1.5') {
			command = 'OcppSoap15';
		} else if (chargebox.type.msg === 'json' && chargebox.type.version === '1.6') {
			command = 'OcppJson16';
		}
		command += 'RemoteStartTransaction';
		payload = {
			user: 'ocpi',
			transaction: {
				chargebox: filter.location_id,
				evse_uid: filter.evse_uid,
				connector: parseInt(filter.connector_id),
				payment: 'ocpi',
				response_url: filter.response_url,
			},
		};
		console.log('Payload', payload);
	}
	if (event.path.includes('RESERVE_NOW')) {
		let chargebox = await DynamoDBChargeboxes.get(filter.location_id);
		console.log('Chargebox', chargebox);
		if (chargebox.type.msg === 'soap' && chargebox.type.version === '1.5') {
			command = 'OcppSoap15';
		} else if (chargebox.type.msg === 'json' && chargebox.type.version === '1.6') {
			command = 'OcppJson16';
		}
		command += 'ReserveNow';
		payload = {
			reservation: {
				chargebox: filter.location_id,
				connector: parseInt(filter.connector_id),
				payment: 'ocpi',
				expiry_date: filter.expiry_date,
			},
			user: 'ocpi',
		};
	}
	if (event.path.includes('CANCEL_RESERVATION')) {
		command += 'CancelReservation';
	}
	if (event.path.includes('STOP_SESSION')) {
		command += 'RemoteStopTransaction';
	}
	if (event.path.includes('UNLOCK_CONNECTOR')) {
		command += 'UnlockConnector';
	}

	try {
		console.log('Command', command, payload);
		let invocation = await Lambda.invoke(command, payload);
		console.log('Invocation', invocation);
		let invPayload = JSON.parse(invocation.Payload);
		let result = {};
		if (invPayload.errorMessage) {
			result = {
				result: 'FAILED',
			};
		} else if (invPayload.idTag) {
			result = {
				result: 'ACCEPTED',
			};
		}

		return httpResult(1000, result);
	} catch (err) {
		console.log(err);
		return httpResult(500, { message: 'InternalServerError' });
	}
};
