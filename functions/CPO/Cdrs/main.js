const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBTransactionsFinished = require('./DynamoDB/transactions-finished');
//const DynamoDBClients = require('./DynamoDB/clients');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	console.log(`*** ${code} - ${bodyStr} ***`);
	let header = {
		'X-Total-Count': 50,
		'X-Limit': 10,
		'Content-Type': 'application/json',
		'Content-Length': Buffer.byteLength(bodyStr),
	};
	return {
		statusCode: code,
		headers: header,
		body: bodyStr,
	};
};

const getFinishedTransactions = async (filter) => {
	let transactions = await DynamoDBTransactionsFinished.getFinishedTransactions(filter);
	let chargeboxes = await DynamoDBChargeboxes.getAll();
	let charges = [];
	chargeboxes.forEach((charge) => {
		charges[charge.id] = charge;
	});
	let chargeSelected = {};
	let items = [];
	if (transactions.length > 0) {
		transactions.forEach((transaction) => {
			let datasCharge = transaction.id.split('__');
			let connector = datasCharge[1];
			chargeSelected = charges[transaction.chargebox];
			if (!chargeSelected) {
				return;
			}
			items.push({
				country_code: 'IT',
				party_id: 'THO',
				id: transaction.id,
				start_date_time: new Date(transaction.date_started),
				end_date_time: new Date(transaction.date_finished),
				cdr_token: transaction.id_tag,
				auth_method: 'WHITELIST',
				cdr_location: {
					id: chargeSelected.id,
					name: chargeSelected.alias,
					address: chargeSelected.address,
					city: chargeSelected.city,
					postal_code: chargeSelected.postal_code ? chargeSelected.postal_code : '000',
					country: chargeSelected.country,
					coordinates: chargeSelected.coordinates,
					evse_uid: connector,
					evse_id: connector,
					connector_id: connector,
					connnector_standard:
						chargeSelected.connectors[connector] && chargeSelected.connectors[connector].type ? chargeSelected.connectors[connector].type : '',
					connector_format:
						chargeSelected.connectors[connector] && chargeSelected.connectors[connector].format ? chargeSelected.connectors[connector].format : '',
					connector_power_type:
						chargeSelected.connectors[connector] && chargeSelected.connectors[connector].power_type
							? chargeSelected.connectors[connector].power_type
							: '',
					uid: '',
					type: '',
					contract_id: 'ITTHO',
				},
				currency: transaction.payment && transaction.payment.currency ? transaction.payment.currency : '',
				total_cost: transaction.payment && transaction.payment.price ? transaction.payment.price : '',
				total_energy: transaction.meter_stop - transaction.meter_start,
				total_time: (transaction.date_finished - transaction.date_started) / (1000 * 60 * 60),
				last_updated: new Date(transaction.date_finished),
			});
		});
	}
	return items;
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	let filter = event.queryStringParameters;
	try {
		return httpResult(200, await getFinishedTransactions(filter));
	} catch (err) {
		console.log(err);
		return httpResult(500, { message: 'InternalServerError' });
	}
};
