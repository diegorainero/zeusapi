const now = Date.now();

const getTimestamps = (days) => {
	let ret = {};

	for (let day of days) {
		ret[day] = {
			from: now - day * 86400000,
			to: now,
		};
	}

	return ret;
};

exports.getChargesCount = (data, days) => {
	let timestamps = getTimestamps(days);
	let ret = {};
	for (let day of days) {
		ret[`last_${day}`] = 0;
		let { from, to } = timestamps[day];
		for (let item of data) {
			if (item.date_started < from || item.date_started > to || item.date_finished - item.date_started < 180000) {
				continue;
			}
			ret[`last_${day}`]++;
		}
	}
	return ret;
};

exports.getEnergy = (data, days) => {
	let timestamps = getTimestamps(days);
	let ret = {};
	for (let day of days) {
		ret[`last_${day}`] = 0;
		let { from, to } = timestamps[day];
		for (let item of data) {
			if (item.date_started < from || item.date_started > to || item.date_finished - item.date_started < 180000) {
				continue;
			}
			ret[`last_${day}`] += Math.round(item.meter_stop - item.meter_start);
		}
	}
	return ret;
};

exports.getUsersCount = (data, days) => {
	let timestamps = getTimestamps(days);
	let ret = {};
	for (let day of days) {
		ret[`last_${day}`] = 0;
		let { from, to } = timestamps[day];
		let users = new Set();
		let idTags = new Set();
		for (let item of data) {
			if (item.date_started < from || item.date_started > to || item.date_finished - item.date_started < 180000) {
				continue;
			}
			switch (item.type) {
				case 'remote':
					if (typeof item.user === 'string') {
						users.add(item.user);
					}
					break;
				case 'local':
					if (typeof item.user === 'string') {
						users.add(item.user);
					} else if (typeof item.id_tag === 'string') {
						idTags.add(item.id_tag);
					}
					break;

				default:
					break;
			}
		}
		ret[`last_${day}`] = users.size + idTags.size;
	}
	return ret;
};

exports.getDisconnections = (data, days) => {
	let timestamps = getTimestamps(days);
	let ret = {};
	for (let day of days) {
		ret[`last_${day}`] = 0;
		let { from, to } = timestamps[day];
		for (let item of data) {
			if (item.timestamp < from || item.timestamp > to) {
				continue;
			}
			ret[`last_${day}`]++;
		}
	}
	return ret;
};
