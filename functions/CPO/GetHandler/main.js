const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	return {
		statusCode: code,
		headers: { 'Content-Type': 'application/json' },
		body: bodyStr,
	};
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));
	try {
		let version = event.resource;
		let ret = [];
		console.log('Version', version);
		if (version.includes('2.1.1')) {
			ret = [
				{
					data: {
						version: '2.1.1',
						endpoints: [
							{
								identifier: 'cdrs',
								role: 'SENDER',
								url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/cdrs',
							},
							{
								identifier: 'commands',
								role: 'SENDER',
								url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/commands',
							},
							{
								identifier: 'credentials',
								role: 'SENDER',
								url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/credentials',
							},
							{
								identifier: 'locations',
								role: 'SENDER',
								url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/locations',
							},
							{
								identifier: 'sessions',
								role: 'SENDER',
								url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/sessions',
							},
							{
								identifier: 'tariffs',
								role: 'SENDER',
								url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/tariffs',
							},
							// {
							// 	identifier: 'tokens',
							// 	role: 'SENDER',
							// 	url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.1.1/tokens',
							// },
						],
					},
					status_code: 1000,
					status_message: 'Success',
					timestamp: Date.now(),
				},
			];
		}
		// if (version.includes('2.2')) {
		// 	ret = [
		// 		{
		// 			data: {
		// 				version: '2.2',
		// 				endpoints: [
		// 					{
		// 						identifier: 'cdrs',
		// 						role: 'SENDER',
		// 						url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.2/cdrs',
		// 					},
		// 					{
		// 						identifier: 'commands',
		// 						role: 'SENDER',
		// 						url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.2/commands',
		// 					},
		// 					{
		// 						identifier: 'credentials',
		// 						role: 'SENDER',
		// 						url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.2/credentials',
		// 					},
		// 					{
		// 						identifier: 'locations',
		// 						role: 'SENDER',
		// 						url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.2/locations',
		// 					},
		// 					{
		// 						identifier: 'sessions',
		// 						role: 'SENDER',
		// 						url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.2/sessions',
		// 					},
		// 					{
		// 						identifier: 'tariffs',
		// 						role: 'SENDER',
		// 						url: 'https://' + process.env.API_URL_ADDR + '/latest/ocpi/cpo/2.2/tariffs',
		// 					},
		// 				],
		// 			},
		// 			status_code: 1000,
		// 			status_message: 'Success',
		// 			timestamp: Date.now(),
		// 		},
		// 	];
		// }
		return httpResult(200, ret);
	} catch (err) {
		console.log(err);
		return httpResult(500, { message: 'InternalServerError' });
	}
};
