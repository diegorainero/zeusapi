const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.get = async (id) => {
	let params = {
		TableName: process.env.DYNAMODB_CHARGEBOXES,
		Key: { id },
		ProjectionExpression: '#id, #active, #alias, #coordinates, #type, #connectors, #connected, #last_heartbeat, #timezone, #public',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#active': 'active',
			'#alias': 'alias',
			'#coordinates': 'coordinates',
			'#type': 'type',
			'#connectors': 'connectors',
			'#connected': 'connected',
			'#last_heartbeat': 'last_heartbeat',
			'#timezone': 'timezone',
			'#public': 'public',
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.get(params).promise();
	return data.Item || null;
};

exports.getByClient = async (geohash, onlyActive) => {
	let ret = [];
	let lastEvaluatedKey = null;
	let params = {
		TableName: process.env.DYNAMODB_CHARGEBOXES,
		ProjectionExpression: '#id, #active, #alias, #coordinates, #type, #connectors, #connected, #last_heartbeat, #timezone, #public',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#alias': 'alias',
			'#coordinates': 'coordinates',
			'#type': 'type',
			'#connectors': 'connectors',
			'#connected': 'connected',
			'#last_heartbeat': 'last_heartbeat',
			'#timezone': 'timezone',
			'#active': 'active',
			'#public': 'public',
		},
		ExpressionAttributeValues: {
			':true': true,
		},
		ReturnConsumedCapacity: 'NONE',
	};

	if (geohash !== null) {
		params.FilterExpression = '#geohash = :geohash';
		params.ExpressionAttributeNames['#geohash'] = 'geohash';
		params.ExpressionAttributeValues[':geohash'] = geohash;
	}
	if (onlyActive) {
		if (params.hasOwnProperty('FilterExpression')) {
			params.FilterExpression += ' and #active = :true';
		} else {
			params.FilterExpression = '#active = :true';
		}
		params.ExpressionAttributeValues[':true'] = true;
	}

	do {
		if (lastEvaluatedKey !== null) {
			params.ExclusiveStartKey = lastEvaluatedKey;
		}
		let data = await DynamoDB.scan(params).promise();
		ret = ret.concat(data.Items || []);
		lastEvaluatedKey = data.LastEvaluatedKey;
	} while (lastEvaluatedKey === null);

	return ret;
};

exports.getByGeoHash = async (geohash, excludeClient, onlyActive) => {
	let ret = [];
	let lastEvaluatedKey = null;
	let params = {
		TableName: process.env.DYNAMODB_CHARGEBOXES,
		IndexName: 'geohash-index',
		KeyConditionExpression: '#geohash = :geohash',
		FilterExpression: '#client <> :client and #public = :true',
		ProjectionExpression: '#id, #active, #alias, #client, #coordinates, #type, #connectors, #connected, #last_heartbeat, #timezone',
		ExpressionAttributeNames: {
			'#id': 'id',
			'#alias': 'alias',
			'#client': 'client',
			'#coordinates': 'coordinates',
			'#type': 'type',
			'#connectors': 'connectors',
			'#connected': 'connected',
			'#last_heartbeat': 'last_heartbeat',
			'#timezone': 'timezone',
			'#geohash': 'geohash',
			'#active': 'active',
			'#public': 'public',
		},
		ExpressionAttributeValues: {
			':true': true,
			':geohash': geohash,
			':client': excludeClient,
		},
		ReturnConsumedCapacity: 'NONE',
	};

	if (onlyActive) {
		params.FilterExpression += ' and #active = :true';
	}

	do {
		if (lastEvaluatedKey !== null) {
			params.ExclusiveStartKey = lastEvaluatedKey;
		}
		let data = await DynamoDB.query(params).promise();
		ret = ret.concat(data.Items || []);
		lastEvaluatedKey = data.LastEvaluatedKey;
	} while (lastEvaluatedKey === null);

	return ret;
};
