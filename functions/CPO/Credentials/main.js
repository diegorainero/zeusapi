const geohash = require('ngeohash');
const http = require('http');
const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBTransactionsRunning = require('./DynamoDB/transactions-running');
const DynamoDBReservations = require('./DynamoDB/reservations');
const DynamoDBOcpiToken = require('./DynamoDB/ocpi-token');

function maketoken(length) {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	console.log(`*** ${code} - ${bodyStr} ***`);
	return {
		statusCode: code,
		headers: { 'Content-Type': 'application/json' },
		body: bodyStr,
	};
};

const request = (payload) =>
	new Promise((resolve, reject) => {
		let body =
			'{  "url": "https://dev.zeus.thor.tools/latest/ocpi/versions",  "token": "kjNq1y7OTg4hCiV9MCTCB9icDYetcgNy5Xie7AST",  "party_id": "THO",  "country_code": "IT",  "business_details": {    "name": "Smartec"  }}';
		let postData = JSON.stringify(body);
		console.log('Payload', payload);
		let req = http.request(payload, (res) => {
			let output = '';
			res.on('data', (chunk) => (output += chunk));
			res.on('end', () => {
				console.log(`request sent to client`);
				console.log(`${res.statusCode} - ${res.statusMessage} - ${output}`);
				resolve(output);
			});
		});
		req.on('error', (e) => {
			console.log(`Error while sending an request to the client API endpoint`);
			console.log(e);
			reject(e);
		});
		req.write(postData);
		req.end();
	})
		.then(function (data) {
			console.log('DATA', data);
			return JSON.parse(data);
		})
		.catch(function (err) {
			console.log('Err.', err);
		});

exports.handler = async (event) => {
	let body = JSON.parse(event.body);
	console.log(body);
	try {
		if (body.token) {
			let data = {
				id: body.token,
				type: 'OUT',
			};
			await DynamoDBOcpiToken.add(data);
		}

		let versions_payload = {
			method: 'GET',
			port: 8080,
			hostname: 'cpo-testing.evway.net',
			path: '/ocpi/emsp',
			headers: {
				Authorization: 'Token ' + body.token,
				'Content-Type': 'application/json',
			},
		};
		let versions = await request(versions_payload);
		//let onlyActive = true;
		//let coordinates = null;

		console.log('Versions', versions.data);
		let endpoints_payload = {
			method: 'GET',
			port: 8080,
			hostname: 'cpo-testing.evway.net',
			path: '/ocpi/emsp/2.1.1',
			headers: {
				Authorization: 'Token ' + body.token,
				'Content-Type': 'application/json',
			},
		};
		let endpoints = await request(endpoints_payload);
		console.log('EndPoints', endpoints.data);

		let token_a = await DynamoDBOcpiToken.get(body.token);
		console.log('TOKEN A', token_a);
		let token_c = '9e80ae10-28be-11e9-b210-d663bd873d94';
		if (token_a && token_a.type === 'A') {
			let data = {
				id: token_c,
				type: 'IN',
			};
			await DynamoDBOcpiToken.add(data);
			await DynamoDBOcpiToken.delete(token_a);
		} else {
			console.log('C', token_c);
			let tmp = await DynamoDBOcpiToken.get(token_c);
			console.log('Tmp', tmp);
			if (tmp && tmp !== null) {
				token_c = tmp.id;
			} else {
				return httpResult(3001, { message: 'Error TOKEN' });
			}
		}
		console.log('TOKEN_C', token_c);
		return httpResult(200, {
			token: token_c,
			url: 'https://dev.zeus.thor.tools/latest/ocpi/cpo/versions',
			roles: [
				{
					role: 'CPO',
					party_id: 'THO',
					country_id: 'IT',
				},
			],
		});
		//return await getChargeboxes(onlyActive, coordinates);
	} catch (err) {
		console.log(err);
		return httpResult(3001, { message: 'InternalServerError' });
	}
};
