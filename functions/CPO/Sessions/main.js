const DynamoDBChargeboxes = require('./DynamoDB/chargeboxes');
const DynamoDBTransactionsRunning = require('./DynamoDB/transactions-running');
const DynamoDBReservations = require('./DynamoDB/reservations');
const DynamoDBClients = require('./DynamoDB/clients');

const httpResult = (code, body) => {
	let bodyStr = JSON.stringify(body);
	console.log(`*** ${code} - ${bodyStr} ***`);
	let header = {
		'X-Total-Count': 50,
		'X-Limit': 10,
		'Content-Type': 'application/json',
		'Content-Length': Buffer.byteLength(bodyStr),
	};
	return {
		statusCode: code,
		headers: header,
		body: bodyStr,
	};
};

const request = (url, payload) =>
	new Promise((resolve, reject) => {
		let body = {
			id: 1,
			token: 'kjNq1y7OTg4hCiV9MCTCB9icDYetcgNy5Xie7AST',
			locationd_id: 'THO',
			authorization_reference: '123222222',
		};
		let postData = JSON.stringify(body);
		console.log('Payload', payload);
		let req = http.request(payload, (res) => {
			let output = '';
			res.on('data', (chunk) => (output += chunk));
			res.on('end', () => {
				console.log(`request sent to client`);
				console.log(`${res.statusCode} - ${res.statusMessage} - ${output}`);
				resolve(output);
			});
		});
		req.on('error', (e) => {
			console.log(`Error while sending an request to the client API endpoint`);
			console.log(e);
			reject(e);
		});
		req.write(postData);
		req.end();
	})
		.then(function (data) {
			console.log('DATA', data);
			return JSON.parse(data);
		})
		.catch(function (err) {
			console.log('Err.', err);
		});

const getRunningTransactions = async (filter) => {
	let transactions = await DynamoDBTransactionsRunning.getTransactions();
	//let chargebox = await DynamoDBChargeboxes.get(transactions.chargebox);
	console.log('Transactions', transactions);
	let item = [];
	if (transactions.length > 0) {
		transactions.forEach((transaction) => {
			console.log('Started', transaction.started);
			item.push({
				country_code: 'IT',
				party_id: 'THO',
				id: transaction.id,
				start_date_time: new Date(transaction.date_requested),
				kwh: transaction.current_meter - transaction.meter_start,
				cdr_token: {
					uid: transaction.id,
					type: transaction.type,
				},
				auth_method: 'WHITELIST',
				locationd_id: transaction.chargebox,
				evse_id: transaction.connector,
				connector_id: transaction.connector,
				currency: transaction.payment && transaction.payment.currency ? transaction.payment.currency : 'EUR',
				total_cost: {
					incl_vat: transaction.payment && transaction.payment.price ? transaction.payment.price : '0',
				},
				status: transaction.started === true ? 'ACTIVE' : 'PENDING',
				last_updated: new Date(transaction.timestamp).toISOString(),
			});
		});
	}

	return item;
};

exports.handler = async (event) => {
	console.log(JSON.stringify(event));

	try {
		let filter = null;
		return httpResult(200, await getRunningTransactions(filter));
	} catch (err) {
		console.log(err);
		return httpResult(500, { message: 'InternalServerError' });
	}
};
