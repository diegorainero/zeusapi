const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.getReservedConnectors = async (chargebox) => {
	let params = {
		TableName: process.env.DYNAMODB_RESERVATIONS,
		KeyConditionExpression: '#chargebox = :chargebox',
		FilterExpression: '#status = :status and #expiry_date > :expiry_date',
		ProjectionExpression: '#connector',
		ExpressionAttributeNames: {
			'#chargebox': 'chargebox',
			'#status': 'status',
			'#expiry_date': 'expiry_date',
			'#connector': 'connector',
		},
		ExpressionAttributeValues: {
			':chargebox': chargebox,
			':status': 'accepted',
			':expiry_date': Date.now(),
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.query(params).promise();
	return (data.Items || []).map((i) => i.connector);
};
