const AWS = require('aws-sdk');
const DynamoDB = new AWS.DynamoDB.DocumentClient();

exports.getTransactions = async () => {
	let params = {
		TableName: process.env.DYNAMODB_TRANSACTIONS_RUNNING,
		ReturnConsumedCapacity: 'NONE',
	};
	let data = await DynamoDB.scan(params).promise();
	return data.Items || [];
};

exports.getTransactionSession = async (id) => {
	let params = {
		TableName: process.env.DYNAMODB_TRANSACTIONS_RUNNING,
		Key: { id },
		ProjectionExpression: '#chargebox, #connector, #current_meter, #id_tag, #meter_start, #timestamp, #transaction_id, #type, #user, #payment',
		ExpressionAttributeNames: {
			'#chargebox': 'chargebox',
			'#connector': 'connector',
			'#current_meter': 'current_meter',
			'#id_tag': 'id_tag',
			'#meter_start': 'meter_start',
			'#timestamp': 'timestamp',
			'#transaction_id': 'transaction_id',
			'#type': 'type',
			'#started': 'started',
			'#user': 'user',
			'#payment': 'payment',
		},
		ReturnConsumedCapacity: 'NONE',
	};

	let data = await DynamoDB.get(params).promise();
	return data.Item || [];
};
